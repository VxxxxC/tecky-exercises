enum TxnType {
   produce = 1, // register
   transfer = 2, //transfer item ownership
   split = 3, //break down item
   invite = 4, //add authorities
   abolish = 5,// remove authorities
   register = 6, //register
}

type Txn = {
   produce | transfer | split | invite | abolish | register
}

type KeyId = number
type TimeStamp = number
type Url = string
type Hash = string
type Bool = 1 | 0
type PublicKey = Buffer


/*
** Transaction Type **
*/
export type produce = {
   type: TxnType.produce
   owner: KeyId
   item: KeyId
   timestamp: TimeStamp
   meta: Buffer
   meta_type: number
   signature: Hash //signed by owner
}

export type transfer = {
   type: TxnType.transfer
   sender: KeyId
   receiver: KeyId
   meta_type: number
   meta: string
   timestamp: TimeStamp
   signature: Hash //signed by sender
}

export type split = {
   type: TxnType.split
   origin_item: KeyId
   items: {
      owner: KeyId
      item: KeyId
      meta: Hash
   }[]
}
   |
{
   meta_type: number
   meta: string
   timestamp: TimeStamp
   signature: Hash //signed by origin_item owner , transfered receiver
}

export type invite = {
   type: TxnType.invite,
   nomination: Hash,
   signature: Hash //signed by multiply authorities
}

export type abolish = {
   type: TxnType.abolish,
   impeachment: Hash,
   signature: Hash //signed by multiply authorities
}

export type register = {
   type: TxnType.register,
   id: KeyId,
   public_key: PublicKey,
   username: string,
   timestamp: TimeStamp,
   signature: Hash //signed
}

export enum MsgType {
   blockProposal = 1,
   blockProposalResponse = 2,
   blockAnnounceResponse = 3, // when collected enough response

   nominationProposal = 4,
   nominationProposalResponse = 5,
   nominationAnnouncement = 6, //when collected enough response

   impeachProposal = 7,
   impeachProposalResponse = 8,
   impeachAnnouncement = 9, //when collected enough response
}


type ItemMetaData = {
   images: Hash[]
   desc: string
   extra: string
}

type LatLng = [lat: number, lng: number]

export type BlockProposal = {
   type: MsgType.blockProposal,
   prev_hash: Hash,
   sender: KeyId,
   timestamp: TimeStamp,
   txns: Txn[],
}

export type BlockProposalResponse = {
   type: MsgType.blockAnnounceResponse,
   signature: Hash //signed bya authorities
   proposal: Hash // from BlockProposal
   sender: KeyId
   is_accept: Bool,
}

export type Block = {
   sender: KeyId, // same as proposal
   txns: Txn[], // same as proposal
   timestamp: TimeStamp, // same as proposal

   signature: Hash //signed by majority of authorities 
   proposal: Hash
}

export type Nomination = {
   type: MsgType.nominationProposal,
   sender: KeyId,
   target: KeyId,
   timestamp: TimeStamp,
   signature: Hash[] // signed by authorities
}

export type NominationResponse = {
   type: MsgType.nominationProposalResponse,
   sender: KeyId,
   target: KeyId,
   is_accept: Bool,
   timestamp: TimeStamp,
   signature: Hash // signed by authorities
}

export type nominationAnnouncement = {
   type: MsgType.nominationAnnouncement,
   nomination: Hash,
   timestamp: TimeStamp,
   signature: Hash[],
}

export type Impeachment = {
   type: MsgType.impeachProposal,
   sender: KeyId,
   target: KeyId,
   timestamp: TimeStamp,
   signature: Hash // signed by authorities
}

export type ImpeachmentResponse = {
   type: MsgType.impeachProposalResponse,
   sender: KeyId,
   target: KeyId,
   is_accept: Bool,
   timestamp: TimeStamp,
   signature: Hash // signed by authorities
}
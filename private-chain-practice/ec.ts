import forge from 'node-forge';

let ec = forge.pki.ed25519;
console.log({ ec })

let sha256 = forge.md.sha256
console.log({ sha256 })

let keypair = ec.generateKeyPair()
console.log({ keypair })

let { publicKey, privateKey } = keypair;
console.log({ publicKey, privateKey })

let msg = 'abc'

let signature = ec.sign({
   message: msg,
   encoding: 'utf8',
   privateKey: privateKey,
})

// console.log({signature})

console.log("base64 signature: " + signature.toString('base64'))
console.log("hex signature: " + signature.toString('hex'))

let a = ec.publicKeyFromPrivateKey({ privateKey })
console.log("to base64 : " + a.toString('base64'))
console.log("to hex : " + a.toString('hex'))


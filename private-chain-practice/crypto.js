const crypto = require("crypto");

// console.log(crypto);

const { generateKey } = require("crypto");

let keyPair = generateKey("hmac", { length: 64 }, (err, key) => {
  if (err) throw err;
  console.log(key.export().toString("hex")); // 46e..........620
});

// console.log(keyPair);

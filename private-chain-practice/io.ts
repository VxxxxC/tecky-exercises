import { createServer, connect } from 'net';


const PORT = 8100;

let server = createServer(client => {
   console.log('[server] got client', client.remoteAddress)
})

server.listen(PORT, () => {
   let client = connect(PORT, '127.0.0.1', () => {
      console.log('[client] self', client.address())
   })

   client.on('connect', () => {
      console.log('[client] connected')

   })
})
import { createSign, createPrivateKey, createPublicKey } from 'crypto'
import forge from 'node-forge';

let rsa = forge.pki.rsa;
console.log({ rsa })
let md = forge.md
console.log({ md })
let { sha256 } = md;
console.log({ sha256 })


let keypair = rsa.generateKeyPair({ bits: 2048, e: 0x10001 })
console.log({ keypair })

let { publicKey, privateKey } = keypair
console.log({ publicKey, privateKey })

let msg = ('abc')

let digest = sha256.create();
digest.update(msg, 'utf8');
let msgDigest = digest.digest().toHex();
console.log({ msgDigest })

let msgSignature = privateKey.sign(digest)
console.log({ msgSignature })
// let sign = createSign('RSA-SHA256')

let fakeMsg = 'def';

let fakeDigest = sha256.create().update(fakeMsg, 'utf8').digest().toHex();
console.log({ fakeDigest })

console.log(Buffer.from(msgSignature, 'binary').toString('base64'));

let verify = publicKey.verify(fakeDigest, msgSignature)
console.log({ verify })

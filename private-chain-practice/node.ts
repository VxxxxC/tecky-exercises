import { BlockProposal, BlockProposalResponse, MsgType } from './data'

class node {
   onReceivedBlock() { }
   createBlock() { }
}

type BaseMsg = {
   type:MsgType
}

namespace sampleMessages{
   
   export let blockProposal: BlockProposal = {
      type: MsgType,blockProposal,
      sender : 1,
      txns: [],
      timestamp: 1,
   }

   export let blockProposalResponse: BlockProposalResponse = {
      type: MsgType,blockProposalResponse,
      proposal: 'hash',
      is_accept: 1,
      timestamp: 1,
   }
}





export class MessageReceiver {
   constructor(samples: BaseMsg[]){
      console.log(samples)
   }
   
   onMessage(msg: BaseMsg) {
      msg.type
   }
}
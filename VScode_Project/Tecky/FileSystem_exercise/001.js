const fs = require('fs');
// fs.writeFileSync('001.txt', 'This is normal Test txt file')
// fs.writeFileSync('002.txt', 'This is not yet encode Test txt file')

// const normal = fs.readFileSync('001.txt', 'utf8')
// const notEncode = fs.readFileSync('002.txt')
// console.log(normal)
// console.log(notEncode)

// const writeName = readlineSync.question('Please Enter Your Name : ')
// fs.writeFileSync('writeName.txt', writeName)


// let i = fs.readFileSync('CountingRun.txt', 'utf8');
// console.log('Your have run this program ' + i + ' times');
// i++;
// fs.writeFileSync('CountingRun.txt', i); // This code wrote exactly same as instructor, but somehow i just can't run it...why??


const readlineSync = require('/Users/veperho/Desktop/Visual-Studio-Code-Project/VScode_Project/Tecky/npm_json_exercise/node_modules/readline-sync')

// let name = readlineSync.question('what is your name? ')
let existingName = fs.readFileSync('NameNote.txt', 'utf8')
// fs.writeFileSync('NameNote.txt', existingName + `\n` + name)

let nameArray = existingName.split(`\n`) // This is making an array from string in txt file , by using "split"
console.log(nameArray)

nameArray.splice(0,1,'ET') // add or remove content of index by using "splice"
fs.writeFileSync('nameNote.txt', nameArray.join(`\n`)) // This is making back to string for txt file from array data, by using "join"
console.log(existingName)
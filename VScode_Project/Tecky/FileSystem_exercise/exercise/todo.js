const fs = require('fs')
const readlineSync = require('/Users/veperho/Desktop/Visual-Studio-Code-Project/VScode_Project/Tecky/npm_json_exercise/node_modules/readline-sync')


let task = fs.readFileSync('todo.txt', 'utf8').split(`\n`)
console.log(task)

console.log(`You have below tasks`)
for(let i = 0; i < task.length; i++) {
  console.log(`[${i + 1}] ${task[i]}`)
}

let index = readlineSync.question('If you want to add a task, type `+`. If you want to clear a task, type the number (1-' + task.length + '):')
if(index == `+`){
  let newTask = readlineSync.question('what task is it?')
  task.push(newTask)
  fs.writeFileSync('todo.txt', task.join(`\n`))

  console.log(task)
}else{
  let clearTask = parseInt(index)
  if(!isNaN(clearTask)){
    task.splice(clearTask - 1 , 1)
    fs.writeFileSync('todo.txt', task.join(`\n`))
  }
}


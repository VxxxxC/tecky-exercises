let x = 10

while( x > 0 ) {
  console.log(x)
  x -= 1;
}

let y = 0

while( y < 5 ){

  let star = ''
  
  let s = 0 // make a internal loop of the loop, to adding space for push the star to the right 
  while( s < y ){
    star += ' ';
    s += 1;
  }  

  let z = 5 - y // make a internal loop in the loop, for adding more star in each loops
  while( z > 0){
    star += '*';
    z -= 1;
  }

  console.log(star)
  y += 1; // basic looping to adding one star for each loops
}
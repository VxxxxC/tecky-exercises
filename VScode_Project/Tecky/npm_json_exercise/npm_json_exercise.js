const readlineSync = require('readline-sync')

let name = readlineSync.question('Hello, What is your name?')

let gender = ['Male', 'Female']
let askgender = readlineSync.keyInSelect(gender, 'What is your gender?')
console.log('Oh! Hi ' + name + ' ! Nice to meet you!' + 'and you are ' + gender[askgender])

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const add = require('date-fns/add')
const parseISO = require('date-fns/parseISO')
const format = require('date-fns/format')


while(true){
  let date = readlineSync.question('Enter the key date: ') // date from question is a string type
  let dateString = parseISO(date) // using "parse" to change the date type from string to a number

  console.log('100-day celebration: ' + format(add(dateString, {days: 100}),'yyyy-MM-dd'))
  console.log('100-month celebration: ' + format(add(dateString, {months: 100}),'yyyy-MM-dd'))
  console.log('10000000-second celebration: ' + format(add(dateString, {seconds: 10000000}),'yyyy-MM-dd'))
}

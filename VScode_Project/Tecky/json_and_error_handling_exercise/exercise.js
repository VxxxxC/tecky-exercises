let hospital = '{"waitTime":[{"hospName":"Alice Ho Miu Ling Nethersole Hospital","topWait":"Over 2 hours"},{"hospName":"Caritas Medical Centre","topWait":"Over 1 hour"},{"hospName":"Kwong Wah Hospital","topWait":"Over 3 hours"},{"hospName":"North District Hospital","topWait":"Around 1 hour"},{"hospName":"North Lantau Hospital","topWait":"Around 1 hour"},{"hospName":"Princess Margaret Hospital","topWait":"Over 1 hour"},{"hospName":"Pok Oi Hospital","topWait":"Over 2 hours"},{"hospName":"Prince of Wales Hospital","topWait":"Over 1 hour"},{"hospName":"Pamela Youde Nethersole Eastern Hospital","topWait":"Over 1 hour"},{"hospName":"Queen Elizabeth Hospital","topWait":"Over 2 hours"},{"hospName":"Queen Mary Hospital","topWait":"Over 2 hours"},{"hospName":"Ruttonjee Hospital","topWait":"Over 3 hours"},{"hospName":"St John Hospital","topWait":"Around 1 hour"},{"hospName":"Tseung Kwan O Hospital","topWait":"Over 2 hours"},{"hospName":"Tuen Mun Hospital","topWait":"Over 3 hours"},{"hospName":"Tin Shui Wai Hospital","topWait":"Around 1 hour"},{"hospName":"United Christian Hospital","topWait":"Over 5 hours"},{"hospName":"Yan Chai Hospital","topWait":"Over 2 hours"}],"updateTime":"9/3/2020 6:30pm"}'

let hospitalObject = JSON.parse(hospital)
console.log(hospitalObject)

// let waitTime = []
// for(let i of hospitalObject.waitTime){
//   waitTime.push(i.topWait)  
// }


// console.log(waitTime)
// waitTime.replace(/[^0-9\.]+/g, "")




/** below is using "array.map" to get the same result */

let time = hospitalObject.waitTime.map(function(time){ 
  return time.topWait
})


console.log(time)

let totalTime = []
for(let i of time){
  totalTime.push((i.replace(/[^0-9\.]+/g, ""))) // searching on Google , found "replace" function can remove all the words of the array
}
console.log(totalTime)

let final = totalTime.reduce(function(previous, current){
  return previous + parseInt(current) // without "parseInt" , the numbers will just stick together as a string type from array data!! that's why need "parseInt" to convert string to a number type
},0)

console.log(final / totalTime.length)
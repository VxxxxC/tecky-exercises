let content = {
  'Name': 'Peter',
  'Age': 28,
  'Gender': 'Male'
}
console.log(`THis is still a JS object`)
console.log(content) // still a JS object

let objectToJson = JSON.stringify(content) // converted JS object to JSON file, must be uppercase for "JSON"
console.log(`This is changed to Json`)
console.log(objectToJson)





let jsonFromPython = '{"Name":"Chris","Age":25,"Gender":"Female"}'

console.log(`This is Json`)
console.log(jsonFromPython) // still a JSON file import from other

let jsonToObject = JSON.parse(jsonFromPython) // Now , it's converted to JS object (can modify in JS now)
console.log(`This is changed to JS object`)
console.log(jsonToObject)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
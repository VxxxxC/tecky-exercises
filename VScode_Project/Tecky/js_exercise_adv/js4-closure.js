/* function abc(){
  let count = 0;
  return function(){ 
    count += 1;
    console.log(count)
  }
}

// without "return function()", countA and countB will be undefine variable assigned from abc() , it is because a new variable assigned from a function , the data type must be also assign to a function !!!  

let countA = abc()

let countB = abc()

countA()
countA()
countA()
countA()
countA()
countA()

countB()
countB()
countB() */

function createPlayer(){
  let numbers = 0
  return {
    win: function(number){
      for(let i of number){
        numbers = numbers + i
      }
    },
    score: function(){
      return numbers
    }
  
  }
}


let alex = createPlayer();
let gordon = createPlayer();

alex.win([1, 2, 9, 8])
alex.win([3]);
gordon.win([4, 4, 9])
alex.win([0, 0, 0, 1, 3]);
gordon.win([2, 2, 4, 5, 6, 6, 7, 8]);

console.log(`Alex's score: ${alex.score()}`);
console.log(`Gordon's score: ${gordon.score()}`);
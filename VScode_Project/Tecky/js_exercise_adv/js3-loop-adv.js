/**
let person = ['Alex','Matt','John','Vi','Brandon']

for(let i of person){
  console.log(i)
}

let box = {
  a: 2,
  b: 2,
  c: 7,
  d: 1,
  e: 9,

}

for(let i in box){
  console.log(i + ' showing ' + box[i] + ' number')
}
**/

let numbers = `3 5 30 31 43 48
2 11 13 45 46 49
11 14 21 28 37 44
18 29 32 33 36 40
2 20 24 30 32 46
5 17 35 37 42 49
1 24 25 27 31 37
15 17 29 30 34 37
5 10 18 20 28 33
1 22 25 27 31 36`.split(`\n`).join(` `).split(` `)

console.log(numbers)

let checkBox = {}
let checkBox1 = 0
for(let number of numbers) {
  if(checkBox[number] == null){
    checkBox[number] = 0
  }
  checkBox[number] += 1
  console.log(checkBox[number])
}

console.log(checkBox)

let highestNum = 0
let higherOccur = 0

for(let number in checkBox) {
  if(checkBox[number] > higherOccur){
    higherNumber = number
    higherOccur = checkBox[number]
  }
}
console.log('number ' + higherNumber + ' is showing the most, It occur ' + higherOccur + ' times')


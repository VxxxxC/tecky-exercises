
 const person = []

person.push({
  'Name': 'Peter',
  'Age': 18,
  'Gender': 'Male'
})
person.push({
  'Name': 'Jade',
  'Age': 15,
  'Gender': 'Female'
})
person.push({
  'Name': 'Matt',
  'Age': 20,
  'Gender': 'Male'
})
person.push({
  'Name': 'Chris',
  'Age': 28,
  'Gender': 'Female'
})
person.push({
  'Name': 'Liana',
  'Age': 18,
  'Gender': 'Female'
})

person.sort(function(personA,personB){ // sort name from A to Z
  if(personA.Name > personB.Name){
    return 1
  }else if(personA.Name < personB.Name){
    return -1
  }else{
    return 0
  }
})
////////////////////////////////////////////////////////////////////////////////////////

console.log( '\n' )
console.log('Below is using "findIndex" to find the index number')

console.log(person.findIndex(function(people){
  while(people.Gender == 'Male'){
    return true
  }
}))
////////////////////////////////////////////////////////////////////////////////////////

console.log( '\n' )
console.log('Below is using "some" to return true of Age')

console.log(person.some(function(people){
  if(people.Age > 18){
    return true
  }
}))
////////////////////////////////////////////////////////////////////////////////////////

console.log( '\n' )
console.log('Below is using "find" to find Chris object')

console.log(person.find(function(people){
  if(people.Name == 'Chris'){
    return true
  }else{
    return false
  }
}))
////////////////////////////////////////////////////////////////////////////////////////

console.log( '\n' )
console.log('Below is using "map" for Name')

console.log(person.map(function(people){
  return people.Name
}))
////////////////////////////////////////////////////////////////////////////////////////

console.log( '\n' )
console.log('Below is using "filter" for Age')

console.log(person.filter(function(people){
  return people.Age <= 18
}))
////////////////////////////////////////////////////////////////////////////////////////
console.log( '\n' )
console.log('Below is using "while loop" to print the array data')

let x = 0 // using while loop method to scan the data or object in array
while( x < person.length){
  console.log(person[x])
  x += 1
}

////////////////////////////////////////////////////////////////////////////////////////
console.log( '\n' )
console.log('Below is using "filter" + "map"')

console.log(person.filter(function(people){
  return people.Age <= 18
}).map(function(people){
  return people.Name
}))

////////////////////////////////////////////////////////////////////////////////////////
console.log( '\n' )
console.log('Below is using "reduce" to get the sum of Age')

console.log(person.reduce(function(previous, current){
  return previous + current.Age
},0))

////////////////////////////////////////////////////////////////////////////////////////
console.log( '\n' )
console.log('Below is using "reduce" to get the Name of smallest of Age')

console.log(person.reduce(function(previous, current){
  if(current.Age < previous){
    return current.Name
  }else{
    return previous
  }
},100))



/* 
const weather = 
[
  {"date": "1 Jan", "minTemperature": 11, "maxTemperature": 18},
  {"date": "2 Jan", "minTemperature": 10, "maxTemperature": 17},
  {"date": "3 Jan", "minTemperature": 12, "maxTemperature": 17},
  {"date": "4 Jan", "minTemperature": 14, "maxTemperature": 19},
  {"date": "5 Jan", "minTemperature": 16, "maxTemperature": 21},
  {"date": "6 Jan", "minTemperature": 17, "maxTemperature": 21},
  {"date": "7 Jan", "minTemperature": 18, "maxTemperature": 22},
]
console.log(weather)

////////////////////////////////////////////////////////////////////////////////////////
console.log( '\n' )
console.log('Below is using "sort" to descending order for minimum temperature')

// clone a new weather array , for showing sorting only
const newWeather = weather.slice()


newWeather.sort(function(weatherA, weatherB){
  if(weatherA.minTemperature < weatherB.minTemperature){
    return 1
  }else if(weatherA.minTemperature > weatherB.minTemperature){
    return -1
  }else{
    return 0
  }
})


// newWeather.sort(function(weatherA, weatherB){ // same result like above but more simple coding
//   return weatherB.minTemperature - weatherA.minTemperature
// })

console.log(newWeather)

////////////////////////////////////////////////////////////////////////////////////////

console.log( '\n' )
console.log('Below is using "map" to get the date from array only')

console.log(weather.map(function(weather){
  return weather.date
}))

////////////////////////////////////////////////////////////////////////////////////////
console.log( '\n' )
console.log('Below is using "filter" to get array of higher temperatures only')

console.log(weather.filter(function(weather){
  return weather.maxTemperature > 20
}))

////////////////////////////////////////////////////////////////////////////////////////
console.log( '\n' )
console.log('Below is using "reduce" to find the date of lowest temperature')

console.log(weather.reduce(function(x, y){
  if(y.minTemperature < x.minTemperature){
    return y
  }else{
    return x
  }
},{ date: '', minTemperature: 100, maxTemperature: 100}).date)
 */
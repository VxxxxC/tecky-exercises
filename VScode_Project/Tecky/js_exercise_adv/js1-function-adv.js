function numLoop(num){
  let x = 0
  let arr = []

  while( x < num ){
    arr.push(x)
    x += 1
  }
  return arr
}

console.log(numLoop(7))
console.log(numLoop(18))

///////////////////////////////////////////////////////////////////////////////

let colleagues = []

colleagues.push({
  'Name': 'Alex',
  'Years of work': 6,
  'Performance': 'Poor',
  'Monthly salary': 10000
})
colleagues.push({
  'Name': 'Gordon',
  'Years of work': 5.5,
  'Performance': 'Good',
  'Monthly salary': 40000
})
colleagues.push({
  'Name': 'Michael',
  'Years of work': 3,
  'Performance': 'Good',
  'Monthly salary': 80000
})
colleagues.push({
  'Name': 'Jason',
  'Years of work': 7,
  'Performance': 'Good',
  'Monthly salary': 70000
})
colleagues.push({
  'Name': 'Brian',
  'Years of work': 0.5,
  'Performance': 'Good',
  'Monthly salary': 20000
})

let checkBox = []

function yearEndBonus(year, perform, salary){
if(year >= 5 && perform == 'Good'){
    checkBox.push(salary*2)
    return salary * 2
  }else  if(year >= 1){
    checkBox.push(salary*1)
    return salary * 1
  }
  return 0
}

yearEndBonus(1, 'Good', 10000) 
yearEndBonus(8, 'Good', 80000) 
yearEndBonus(3, 'Poor', 50000) 
yearEndBonus(5, 'Good', 60000)


console.log(checkBox)
console.log(
  yearEndBonus(1, 'Good', 10000) +
  yearEndBonus(8, 'Good', 80000) +
  yearEndBonus(3, 'Poor', 50000) +
  yearEndBonus(5, 'Good', 60000)
)
/* 
DNA RNA
G   C
C   G
T   A
A   U
*/

let finalString = ''

function rnaTranscription(word){
  let rna = ''

  for(let dna of word){
    if(dna == 'G'){
      dna = 'C'
    }else if(dna == 'C'){
      dna = 'G'
    }else if(dna == 'T'){
      dna = 'A'
    }else if(dna == 'A'){
      dna = 'U'
    }
  rna += dna
  }
  return rna
}

console.log(rnaTranscription("GCTAGCT")) //output "CGAUCGA"
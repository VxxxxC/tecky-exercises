
function checkYear(year){
  if(year % 4 == 0 && year % 100 !== 0){
    console.log('this is leap year!')
  }else if(year % 400 == 0 && year % 100 !== 0){
    console.log('this is leap year!')
  }else if(year % 400 == 0){
    console.log('this is leap year')
  }else{
    console.log('this not a leap year....')
  }
}

checkYear(1960)
checkYear(2060)
checkYear(2040)

checkYear(1980)
checkYear(2020)
import dotenv from 'dotenv';

const envConfig = dotenv.config()
if(envConfig.error){
  console.log("database connecting error...: ", envConfig.error)
}else{
  console.log("database connected with config : ", envConfig)
}
console.log(envConfig)

export let env = {
  DB_NAME: process.env.DB_NAME,
  DB_USER: process.env.DB_USER,
  DB_PASSWORD: process.env.DB_PASSWORD,
}
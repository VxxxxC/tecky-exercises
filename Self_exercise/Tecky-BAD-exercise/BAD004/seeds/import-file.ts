import { Knex } from "knex";
import { file } from "../import"

type Files = {
    name: string;
    Content: string;
    is_file: boolean;
    category: string;
    owner: string;
}



export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("file").del();

    // Inserts seed entries
    for (let files of file) {
        let eachFiles: Files[] = [];
        eachFiles.push(files as any)

        try {
            await knex("file").insert(eachFiles);
            console.log("inserting files :", eachFiles);
        }
        catch (err) {
            console.error({ err })
        }
    }
};

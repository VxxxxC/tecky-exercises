import { Knex } from "knex";
import { user } from "../import"

type Users = {
    username: string;
    password: string;
    level: string;
}



export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();

    // Inserts seed entries
    for (let users of user) {
        let eachUsers: Users[] = [];
        eachUsers.push(users as any)

        try {
            await knex("users").insert(eachUsers);
            console.log("inserting users :", eachUsers);
        }
        catch (err) {
            console.error({ err })
        }
    }
};

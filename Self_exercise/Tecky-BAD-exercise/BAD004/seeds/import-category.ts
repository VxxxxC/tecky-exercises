import { Knex } from "knex";
import { category } from "../import"

type Cats = {
    name: string;
}



export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("category").del();

    // Inserts seed entries
    for (let cata of category) {
        let eachCats: Cats[] = [];
        eachCats.push(cata as any)

        try {
            await knex("category").insert(eachCats);
            console.log("inserting category :", eachCats);
        }
        catch (err) {
            console.error({ err })
        }
    }
};

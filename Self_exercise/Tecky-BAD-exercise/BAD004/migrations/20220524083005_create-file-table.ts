import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('file', (table) => {
    table.increments('id');
    table.string('name').notNullable();
    table.text('Content');
    table.boolean('is_file').notNullable();
    table.string('category').notNullable();
    table.string('owner').notNullable();
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('file');
}


import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('category', (table) => {
    table.increments('id');
    table.string('name');
    table.string('Important');
    table.string('Urgent')
    table.string('Useful')
    table.string('Not Urgent')
    table.string('Not Important')
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('category');
}


import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {

  // Go to https://www.wikipedia.org/
  await page.goto('https://www.wikipedia.org/');

  // Click input[name="search"]
  await page.locator('input[name="search"]').click();

  // Fill input[name="search"]
  await page.locator('input[name="search"]').fill('braille');

  // Press Enter
  await page.locator('input[name="search"]').press('Enter');
  await expect(page).toHaveURL('https://en.wikipedia.org/wiki/Braille');

  // Click text=CreatorLouis Braille >> a
  await page.locator('text=CreatorLouis Braille >> a').click();
  await expect(page).toHaveURL('https://en.wikipedia.org/wiki/Louis_Braille');

  // Click #cite_note-27 cite a
  await page.locator('#cite_note-27 cite a').click();
  await expect(page).toHaveURL('https://www.avh.asso.fr/fr/lassociation/restez-informes/actualites?var=titre&infos=432');

  // Go to https://en.wikipedia.org/wiki/Louis_Braille
  await page.goto('https://en.wikipedia.org/wiki/Louis_Braille');

  // Go to https://www.biography.com/scholar/louis-braille
  await page.goto('https://www.biography.com/scholar/louis-braille');

});
# %%
import numpy as np
from scipy.stats  import mode
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

# %% 
class KNNClassifier:

    def __init__(self, X_train, y_train, n_neighbors = 3):
        self.n_neighbors = n_neighbors
        self._X_train = X_train
        self._y_train = y_train

    """calculate the euclidean distance here"""
    def euclidean_dist(self, point_1, point_2):
        pass

    """accept multiple inputs here and predict one by one with predict_single()"""
    def predict(self, X_test_array):
        pass

    """predict single input here"""
    def predict_single(self, input_data_single):
        pass

# %%
iris = load_iris()
X = iris.data
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# %%
print(X_train.shape)    # (120, 4)
print(X_test.shape)     # (30, 4)
print(y_train.shape)    # (120,)
print(y_test.shape)     # (30,)

# %%
iris_knn_classifier = KNNClassifier(X_train, y_train)
# y_pred = iris_knn_classifier.predict(X_test)
# print(classification_report(y_test, y_pred, target_names=['Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']))

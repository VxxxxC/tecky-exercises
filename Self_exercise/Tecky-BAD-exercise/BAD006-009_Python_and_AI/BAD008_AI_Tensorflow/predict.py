# %%
import tensorflow as tf
from sanic import Sanic
from sanic.response import json
import numpy as np

# %%
model = tf.saved_model.load('./model')
class_names = ['Iris setosa', 'Iris versicolor', 'Iris virginica']


# %%
predict_dataset = tf.convert_to_tensor([
    [5.1, 3.3, 1.7, 0.5, ],
    [5.9, 3.0, 4.2, 1.5, ],
    [6.9, 3.1, 5.4, 2.1]
])

# training=False is needed only if there are layers with different
# behavior during training versus inference (e.g. Dropout).
predictions = model(predict_dataset, training=False)

for i, logits in enumerate(predictions):
    class_idx = tf.argmax(logits).numpy()
    p = tf.nn.softmax(logits)[class_idx]
    name = class_names[class_idx]
    print("Example {} prediction: {} ({:4.1f}%)".format(i, name, 100*p))

# %%
app = Sanic("App_Name")


@app.route("/")
def test(request):
    return json({"hello": "world"})


@app.post("/test")
def callModel(request):
    content = request.json
    predict_dataset = tf.convert_to_tensor(content)
    predictions = model(predict_dataset, training=False)
    results = []
    for i, logits in enumerate(predictions):
        class_idx = tf.argmax(logits).numpy()
        p = tf.nn.softmax(logits)[class_idx]
        name = class_names[class_idx]
        results.append({"name": name, "probability": float(p)})
    return json({"data": results})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)

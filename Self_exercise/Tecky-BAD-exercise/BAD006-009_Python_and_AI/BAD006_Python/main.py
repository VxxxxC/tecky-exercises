#%%
def sum(num):
  x = 2
  return x + num

#%%
sum(10)

#%%
dic = []
user = {
  'name' : 'Matt',
  'age' : 18,
  'gender' : 'male'
}

dic.append(user)
print(dic)

#%%
list = ["appel", "orange", "watermelon"]
print(list)
list.append("melon")
print(list[1::2])

#%%
tuple_list = ("appel", "orange", "watermelon")
print(tuple_list)
print(tuple_list[0::2])

# %%
set_list = {"apple", "orange", "watermelon"}
print(set_list)
set_list.add("melon")
print(set_list)


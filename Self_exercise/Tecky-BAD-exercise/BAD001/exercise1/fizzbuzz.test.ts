
function fizzbuzz(number: any): any {
  let x: any = 0;
  let arr = [];
  let final = [];
  while (x < number) {
    x = x + 1;
    arr.push(x);
  }

  for (let i of arr) {
    if (i % 15 == 0) {
      i = 'fizzbuzz';
    } else if (i % 5 == 0) {
      i = 'buzz';
    } else if (i % 3 == 0) {
      i = 'fizz';
    }
    // console.log(i)
    final.push(i)
  }
  return final.join().split(',').toString()
}
fizzbuzz(100)



describe('fizzbuzz() Testsuit', () => {

  test('if the number is multiply by 3', () => {
    expect(fizzbuzz('6')).toMatch(/fizz/)
  })

  test('if the number is multiply by 5', () => {
    expect(fizzbuzz('10')).toMatch(/buzz/)
  })

  test('if the number is multiply by 15', () => {
    expect(fizzbuzz('30')).toMatch(/fizzbuzz/)
  })
})

function factorial(num: number): number {
  if (num == 0 || num == 1) {
    return 1;
  }
  return factorial(num - 1) * num
}

function fibonacci(num: number): number {
  if (num == 1 || num == 2) {
    return 1;
  }
  return fibonacci(num - 1) + fibonacci(num - 2)
}

describe('factorial() Testsuit())', () => {
  test('num multiply by num times', () => {
    expect(factorial(5)).toBe(120)
  })
})

describe('fibonacci() Testsuit()', () => {
  test('num sum by first two num', () => {
    expect(fibonacci(10)).toBe(55)
  })
})
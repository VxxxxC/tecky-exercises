import fs from "fs";
import path from "path";

function listAllJs(dirPath: string) {
  // console.log(currentPath)
  let jsContainer: string[] = [];

  fs.readdir(dirPath.toString(), (err, files) => {
    // console.log(files)
    if (err) {
      console.log("This is Folder Error : " + err);
      return;
    }
    for (let filename of files) {
      let subPath = dirPath + "/" + filename;
      // console.log(jsContainer)

      fs.readdir(subPath.toString(), (err, subFiles) => {

        if (err) {
          console.log("This is SubFolder Error : " + err);
          return;
        }

        for (let subFileName of subFiles) {

          jsContainer.push(subPath + "/" + subFileName);
          console.log(jsContainer);

          for (let jsString of jsContainer) {

            fs.stat(jsString, (err, stats) => {
              if (err) {
                console.log(`we catch an ERROR!! : `, err);
              }

              if ((stats.isFile()) && (path.extname(jsString) === ".ts")) {
                console.log(jsString)
              }
            })
          }
        }
      });
    }
  });
}

listAllJs(
  "/Users/veperho/Desktop/gitlab-repo/tecky-exercise/Self_exercise/Tecky-WSP-exercise/WSP003"
);
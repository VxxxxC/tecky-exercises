import readline from 'readline';

/** 

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
})

rl.question("Please enter input..", (answer: string) => {
  console.log(`Your Entry is ${answer}`)

})

*/

//-------------------readline with promise and .then------------------------

/** 

function readLinefunc(inputP: string): Promise<void> {

  return new Promise((resolve, reject) => {

    const readLineP = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    })

    resolve(
      readLineP.question("What is your promise?", (inputP: string) => {
        console.log(`Dear, your make a ${inputP} promise...wish you luck`)
      })
    )

  })

}

let a = readLinefunc("money")
a.then(function(data){
  console.log(data)
}).catch(function(err){
  console.log(`we got an error : ${err}`)
})

*/

//-----------------readline with async and await--------------

async function asyncReadline() {

  const readlineA = readline.createInterface({
    input: process.stdin,
    output: process.stdin,
  })

  const first = await new Promise((resolve) => {
    readlineA.question("what is your name?", (inputA: string) => {
      return resolve(inputA)
    })
  })

  const second = await new Promise((resolve) => {
    readlineA.question("How old are you?", (inputA: string) => {
      return resolve(inputA)
    })
  })

}

asyncReadline()
  .then(function (data) {
    console.log("Result is : ", data);
  })
  .then(function (nextData) {
    console.log("Result is : ", nextData);
  })

//////////////////////////////////////////////////////////////////////////////////////

/**
const readlineA = readline.createInterface({
  input: process.stdin,
  output: process.stdin,
})

async function asyncReadlineA(){

  const read = await new Promise((resolve) => {
    readlineA.question("what is your name?", (input: string) => {
      resolve(
        console.log(input)
        )
    })
  })
}
async function asyncReadlineB(){

  const read = await new Promise((resolve) => {
    readlineA.question("How ?", (input: string) => {
      resolve(
        console.log(input)
        )
    })
  })
}

asyncReadlineA()
.then(function(data){
  console.log(data);
  return asyncReadlineB()
})
.then(function(nextData){
  console.log(nextData);
})

*/
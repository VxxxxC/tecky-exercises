import fs from "fs";
import path from "path";

function listAllJS(dirPath: any) {
  let currentPath = dirPath;
  // console.log(currentPath)

  fs.readdir(dirPath, (err, dir) => {

    if (err) {
      console.log(`this is an error from fs.readdir : ` + err);
      return;
    }

    //First layer Directory

    let dirArr: any[] = []
    dir.forEach(dirs => {
      dirArr.push(currentPath + "/" + dirs)
    })

    //Second layer directory

    for (let newDir of dirArr) {

      fs.stat(newDir, (err, stats) => {
        if (err) {
          console.log("we catch an ERROR!!: ", err)
        }

        if (stats.isFile()) {
          if (path.extname(newDir) === ".js") {
            console.log(newDir);
          }
        }

        if (stats.isDirectory()) {
          return listAllJS(newDir)
        }
      })

    }
  });
}

listAllJS('/Users/veperho/Desktop/gitlab-repo/tecky-exercise/Self_exercise');


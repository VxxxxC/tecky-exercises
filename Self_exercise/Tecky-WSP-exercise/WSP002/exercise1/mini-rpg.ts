class rpgPlayer {
  private strength: number;
  private name: string;
  hp: number;
  exp: number;

  constructor(strength: number, name: string, hp: number, exp: number) {
    this.strength = strength;
    this.name = name;
    this.hp = hp;
    this.exp = exp;
  }

  attack(monster: rpgMonster) {
    while (monster.hp > 0) {
      let ranNum = Math.floor(Math.random() * 5);
      if (ranNum > 2.5) {
        console.log(`Your attack monster by ${this.strength * (ranNum > 2.5 ? 3 : 1)}! (HP:${monster.hp}) (Critical hit!)`);
      } else {
        console.log(`You attack monster by ${this.strength * (ranNum > 2.5 ? 3 : 1)}! (HP:${monster.hp})`);
      }
      monster.injure(this.strength * (ranNum > 2.5 ? 3 : 1));
      this.gainExperience(this.exp);
    }
  }

  gainExperience(exp: number) {
    this.exp++;
    console.log(`exp: ${this.exp}`);
  }

  getPlayerHp() {
    return this.hp;
  }
  getPlayerStr() {
    return this.strength;
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////

class rpgMonster {
  // Think of how to write injure
  strength: number;
  hp: number;

  constructor(strength: number, hp: number) {
    this.strength = strength;
    this.hp = hp;
  }

  injure(strength: number) {
    if (this.hp >= strength) {
      this.hp -= strength;
    } else {
      this.hp = 0;
    }
  }
}

// Invocations of the class and its methods
const playerA = new rpgPlayer(20, "Gang", 200, 0);
const monsterA = new rpgMonster(18, 150);
playerA.attack(monsterA);
playerA.attack(monsterA);
playerA.attack(monsterA);
playerA.attack(monsterA);
playerA.attack(monsterA);
playerA.attack(monsterA);
// English counterpart: Player attacks monster

interface Attack {
  damage: number;
}

class BowAndArrow implements Attack {
  //Bow and Arrow Attack here
  damage: number;

  constructor(damage: number) {
    this.damage = damage;
  }
}

class ThrowingSpear implements Attack {
  // Throwing Spear Attack here
  damage: number;

  constructor(damage: number) {
    this.damage = damage;
  }
}

class Swords implements Attack {
  // Throwing Spear Attack here
  damage: number;

  constructor(damage: number) {
    this.damage = damage;
  }
}

class MagicSpells implements Attack {
  // Throwing Spear Attack here
  damage: number;

  constructor(damage: number) {
    this.damage = damage;
  }
}

interface characters {
  hp: number;
  strength: number;
  exp: number;
  primary: Attack;
  secondary: Attack;
}

interface betterRpgPlayer {
  attack(monster: bigMonster): void;
  // switchAttack(): void;
  gainExperience(exp: number): void;
}

//----------------------------Amazon--------------------------------------

class Amazon implements betterRpgPlayer, characters {
  public primary: Attack;
  public secondary: Attack;
  // private usePrimaryAttack: boolean;
  public hp: number;
  public exp: number;
  public strength: number;

  constructor(hp: number, strength: number, exp: number) {
    this.primary = new BowAndArrow(30);
    this.secondary = new ThrowingSpear(40);
    // TODO: set the default value of usePrimaryAttack
    // this.usePrimaryAttack == true;


    this.hp = hp;
    this.strength = strength;
    this.exp = exp;
  }

  attack(monster: bigMonster) {
    while (monster.hp > 0) {
      let weapon = Math.ceil(Math.random() * 5)

      let attackNum = Math.floor(Math.random() * 10)

      if (weapon < 4) {
        if (attackNum > 6) {
          console.log(`Amazon use primary attack monster by CRITICAL!!! ${(this.strength + (this.primary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        } else {
          console.log(`Amazon use primary attack monster by ${(this.strength + (this.primary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        }
        // TODO: use primary attack
        monster.injure(this.strength + (this.primary.damage * (attackNum > 6 ? 3 : 1)))
        this.gainExperience(this.exp)
        console.log(`Your gain exp : ${this.exp}`)
      }

      if (weapon >= 4) {
        if (attackNum > 6) {
          console.log(`Amazon use second attack to monster by CRITICAL ${(this.strength + (this.secondary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        } else {
          console.log(`Amazon use second attack to monster by ${(this.strength + (this.secondary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        }
        // TODO: use secondary attack
        monster.injure(this.strength + (this.secondary.damage * (attackNum > 6 ? 3 : 1)))
        this.gainExperience(this.exp)
        console.log(`Your gain exp : ${this.exp}`)
      }

      return
    }
  }

  injure(strength: number) {
    if (this.hp >= strength) {
      this.hp -= strength;
    } else {
      this.hp = 0;
    }
  }

  // switchAttack() {
  //   let weapon = Math.floor(Math.random()*2)
  //   if(weapon == 0){
  //     return this.primary;
  //   }else{
  //     return this.secondary;
  //   }
  //   // TODO: Change the attack mode for this player
  // }

  //.. The remaining methods.

  gainExperience(exp: number) {
    this.exp++;
  }
}

//------------------------------------------------Paladin-----------------------------------------

class Paladin implements betterRpgPlayer, characters {
  public primary: Attack;
  public secondary: Attack;
  // private usePrimaryAttack: boolean;
  public hp: number;
  public exp: number;
  public strength: number;

  constructor(hp: number, strength: number, exp: number) {
    this.primary = new Swords(50);
    this.secondary = new MagicSpells(25);
    // TODO: set the default value of usePrimaryAttack
    // this.usePrimaryAttack == true;


    this.hp = hp;
    this.strength = strength;
    this.exp = exp;
  }

  attack(monster: bigMonster) {
    while (monster.hp > 0) {
      let weapon = Math.ceil(Math.random() * 5)

      let attackNum = Math.floor(Math.random() * 10)

      if (weapon < 4) {
        if (attackNum > 6) {
          console.log(`Paladin use primary attack monster by CRITICAL!!! ${(this.strength + (this.primary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        } else {
          console.log(`Paladin use primary attack monster by ${(this.strength + (this.primary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        }
        // TODO: use primary attack
        monster.injure(this.strength + (this.primary.damage * (attackNum > 6 ? 3 : 1)))
        this.gainExperience(this.exp)
        console.log(`Your gain exp : ${this.exp}`)
      }

      if (weapon >= 4) {
        if (attackNum > 6) {
          console.log(`Paladin use second attack to monster by CRITICAL ${(this.strength + (this.secondary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        } else {
          console.log(`Paladin use second attack to monster by ${(this.strength + (this.secondary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        }
        // TODO: use secondary attack
        monster.injure(this.strength + (this.secondary.damage * (attackNum > 6 ? 3 : 1)))
        this.gainExperience(this.exp)
        console.log(`Your gain exp : ${this.exp}`)
      }

      return
    }
  }

  injure(strength: number) {
    if (this.hp >= strength) {
      this.hp -= strength;
    } else {
      this.hp = 0;
    }
  }

  // switchAttack() {
  //   let weapon = Math.floor(Math.random()*2)
  //   if(weapon == 0){
  //     return this.primary;
  //   }else{
  //     return this.secondary;
  //   }
  //   // TODO: Change the attack mode for this player
  // }

  //.. The remaining methods.

  gainExperience(exp: number) {
    this.exp++;
  }
}

//------------------------------------------------Barbarian-----------------------------------------------
class Barbarian implements betterRpgPlayer, characters {
  public primary: Attack;
  public secondary: Attack;
  // private usePrimaryAttack: boolean;
  public hp: number;
  public exp: number;
  public strength: number;

  constructor(hp: number, strength: number, exp: number) {
    this.primary = new Swords(55);
    this.secondary = new ThrowingSpear(30);
    // TODO: set the default value of usePrimaryAttack
    // this.usePrimaryAttack == true;


    this.hp = hp;
    this.strength = strength;
    this.exp = exp;
  }

  attack(monster: bigMonster) {
    while (monster.hp > 0) {
      let weapon = Math.ceil(Math.random() * 5)

      let attackNum = Math.floor(Math.random() * 10)

      if (weapon < 4) {
        if (attackNum > 6) {
          console.log(`Barbarian use primary attack monster by CRITICAL!!! ${(this.strength + (this.primary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        } else {
          console.log(`Barbarian use primary attack monster by ${(this.strength + (this.primary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        }
        // TODO: use primary attack
        monster.injure(this.strength + (this.primary.damage * (attackNum > 6 ? 3 : 1)))
        this.gainExperience(this.exp)
        console.log(`Your gain exp : ${this.exp}`)
      }

      if (weapon >= 4) {
        if (attackNum > 6) {
          console.log(`Barbarian use second attack to monster by CRITICAL ${(this.strength + (this.secondary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        } else {
          console.log(`Barbarian use second attack to monster by ${(this.strength + (this.secondary.damage * (attackNum > 6 ? 3 : 1)))}, monster hp : ${monster.hp}`)
        }
        // TODO: use secondary attack
        monster.injure(this.strength + (this.secondary.damage * (attackNum > 6 ? 3 : 1)))
        this.gainExperience(this.exp)
        console.log(`Your gain exp : ${this.exp}`)
      }

      return
    }
  }

  injure(strength: number) {
    if (this.hp >= strength) {
      this.hp -= strength;
    } else {
      this.hp = 0;
    }
  }

  // switchAttack() {
  //   let weapon = Math.floor(Math.random()*2)
  //   if(weapon == 0){
  //     return this.primary;
  //   }else{
  //     return this.secondary;
  //   }
  //   // TODO: Change the attack mode for this player
  // }

  //.. The remaining methods.

  gainExperience(exp: number) {
    this.exp++;
  }
}

//--------------------------------------------------Monster-------------------------------------------------

interface betterRpgMonster {
  attack(player: betterRpgPlayer): void;
}

class bigMonster implements betterRpgMonster {
  // Think of how to write injure
  public strength: number;
  public hp: number;

  constructor(hp: number, strength: number) {
    this.strength = strength;
    this.hp = hp;
  }

  attack(player: Amazon): void {
    while (player.hp > 0) {
      let weapon = Math.floor(Math.random() * 2)
      console.log(weapon)
      if (null) {
        // TODO: use primary attack
      } else {
        // TODO: use secondary attack
      }
    }
  }

  injure(strength: number) {
    if (this.hp >= strength) {
      this.hp -= strength;
    } else {
      this.hp = 0;
    }
  }
}

const Ama = new Amazon(350, 100, 0);
const Pala = new Paladin(500, 120, 0);
const Bar = new Barbarian(550, 150, 0)

const monster = new bigMonster(5000, 100);

Ama.attack(monster);
Pala.attack(monster);
Bar.attack(monster);
Ama.attack(monster);
Pala.attack(monster);
Bar.attack(monster);
Ama.attack(monster);
Pala.attack(monster);
Bar.attack(monster);
Ama.attack(monster);
Pala.attack(monster);
Bar.attack(monster);
Ama.attack(monster);
Pala.attack(monster);
Bar.attack(monster);
Ama.attack(monster);
Pala.attack(monster);
Bar.attack(monster);
Ama.attack(monster);
Pala.attack(monster);
Bar.attack(monster);
Ama.attack(monster);
Pala.attack(monster);
Bar.attack(monster);
Ama.attack(monster);
Pala.attack(monster);
Bar.attack(monster);
Ama.attack(monster);
Pala.attack(monster);
Bar.attack(monster);
let button = document.querySelector(".fetch");

button.addEventListener("click", async () => {
  console.log("fetch button clicked!");
  const res = await fetch("/message");
  const message = await res.json();
  console.log(message);
});

import express from 'express';
import { Client } from 'pg'

const client = new Client({
  user: 'veper',
  password: 'abc123',
  database: 'memowall',
  port: 8080
});

client.connect(err => {
  if (err) console.log(err)
  else console.log('database connected!')
})

const app = express();

app.use(express.json())
app.use(express.urlencoded())

// app.use(express.static('bye'))
app.use(express.static('public'))


app.get('/', (req, res) => {
  console.log(req.body)
  res.end('hi vepor')
})

// username, password
app.post('/register', async (req, res) => {
  const user = req.body
  let result = await client.query('INSERT INTO users (username, password) VALUES ($1, $2) returning id, username', [user.username, user.password])
  let returningUser = result.rows[0]
  console.log(returningUser)
  res.json(returningUser)
})

app.get('/message', (req, res) => {
  res.json({ message: "hi" })
})

app.listen(8964, () => {
  console.log('server started!')
})
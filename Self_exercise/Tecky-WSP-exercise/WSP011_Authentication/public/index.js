const socket = io.connect();

socket.on("toClient", (msg) => {
  console.log(msg);
});

socket.emit("toServer", "this is a client broadcast to server");

document.querySelector("#register").addEventListener("submit", async () => {
  console.log("register submitted!");
  const res = await fetch("/register");
  const submit = await res.json();
  console.log(submit);
});

document.querySelector("#admin").addEventListener("submit", async () => {
  console.log("admin login request sent!");
  const res = await fetch("/admin_login");
  const submit = await res.json();
  console.log(submit);
});
document.querySelector("#user").addEventListener("submit", async () => {
  console.log("user login request sent!");
  const res = await fetch("/login");
  const submit = await res.json();
  console.log(submit);
});

// document
//   .querySelector("#memowall")
//   .addEventListener("submit", async function (event) {
//     event.preventDefault();

//     // Serialize the Form afterwards
//     const form = event.target;
//     const formData = new FormData();

//     formData.append("memoinput", form.memoinput.value);
//     formData.append("memofile", form.memofile.files[0]);

//     const res = await fetch("/", {
//       method: "POST",
//       body: formData,
//     })
//       .then((response) => {
//         return response;
//       })
//       .catch(function (error) {
//         console.log(error.error());
//       })
//       .then(function (result) {
//         console.log(result);
//       });
//     if (form.filetype === "file") {
//       document.querySelector(".imagebox").innHTML = await res.json();
//     } else {
//       document.querySelector(".textbox").innHTML = await res.json();
//     }
//   });

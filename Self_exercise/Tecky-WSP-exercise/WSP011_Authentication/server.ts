import express from 'express';
import session from 'express-session';
import { print } from 'listening-on'
import formidable from 'formidable';
import http from 'http';
import { Server as ServerIO } from 'socket.io';
import fs from 'fs';
import pg from 'pg';


const PORT = 8080;
const app = express()

// 下面係startup Socket.io
const server = new http.Server(app)
const io = new ServerIO(server)

io.on('connection', (socket) => {
  console.log(`client socket conected to by ID : ${socket.id}`)

  socket.emit('toClient', 'this is a backend broadcasted to frontend')
  socket.on('toServer', (msg) => {
    console.log(msg);
  })
})

// 下面係startup Database
const client = new pg.Client({
  database: 'memowall',
  user: 'veper',
  password: 'abc123',
})

client.connect(err => {
  if (err) {
    console.log(err)
  } else {
    console.log('Database connected!!')
  }
})


app.use(express.static('public'))
// app.use(express.static('/upload'))

app.use(express.urlencoded({
  extended: true
})); // Request ==> body

app.use(express.json()); // Request ==> json

app.use(session({
  secret: 'memo',
  resave: true,
  saveUninitialized: true,
})
)

declare module 'express-session' {
  interface SessionData {
    name?: string;
  }
}

app.post('/register', (req, res, next) => {
  // req.body嘅value 就係 username , password 
  // 而佢地來自HTML form 'register' 入面 input 'text'嘅'name' value , 同 input 'password' 嘅 'name' value

  console.log(req.body)
  const { username, password } = req.body;
  client.query('INSERT INTO users (username, password, created_at, updated_at) VALUES ($1,$2,now(),now())', [username, password]);
  res.send('register form submitted!')
  // next() FIXME:
})

// app.get('/register', (req, res) => {
//   res.redirect('/')
// })

app.post('/admin_login', (req, res) => {
  const { admin_username, admin_password } = req.body;
  console.log(req.body)
  if (!admin_username) {
    res.status(404).send({ error: 'You are missing ADMIN username' })
    return
  }
  if (!admin_password) {
    res.status(404).send({ error: 'You are missing ADMIN password' })
    return
  }

})

let admin = client.query('select * from admin;')
admin.then((result) => {
  console.log(result.rows[0].username)
})


// app.post('/login', (req, res) => {
//   const { username, password } = req.body;
//   console.log(req.body)
//   if (!username) {
//     res.status(404).send({ error: 'You are missing username' })
//     return
//   }
//   if (!password) {
//     res.status(404).send({ error: 'You are missing password' })
//     return
//   }
// })


//製造一個只有admin先可以進入或者瀏覽嘅內容
const isAdmin = (req: express.Request, res: express.Response, next: express.NextFunction) => {

  // if (req.session.adminUsername) {
  //   next()
  // } else {
  //   res.status(403).send('You are not authorized')
  // }
}

// 只有係'/admin'呢條route入面，admin(protected) 先會生效
app.use('/admin', isAdmin, express.static('admin')) //Default setting係用'protected'


// const isLoggedIn = (req:express.Request, res:express.Response, next: express.Next) =>{
//   if(req.session.user?){

//   }
// }









const uploadDir = 'upload';
fs.mkdirSync(uploadDir, { recursive: true })
const form = formidable({
  uploadDir,
  keepExtensions: true,
  allowEmptyFiles: false,
  maxFiles: 1,
  maxFileSize: 1024 * 1024 ** 2,
  filter: file => file.mimetype?.startsWith('image/') || false,
})

app.post('/second', (req, res,) => {
  console.log(req.body)
  res.end('Form Submitted!')
})

server.listen(PORT, () => {
  print(PORT)
})


//--------------------Promise of different variable with .then----------------------

function sendString(str: string): Promise<string> {
  return new Promise((resolve, reject) => {
    let newString: string = '';
    newString = newString + str;

    setTimeout(() => {
      resolve(newString)
    }, 1500)

  })
}

let sendStr = sendString('Hi');
sendStr
  .then(function (promiseString) {
    console.log(promiseString)
  })
  .catch(function () {
    console.log("Have ERROR here!!")
  })


let secStr = sendString('World');
secStr
  .then(function (promiseString) {
    console.log(promiseString)
  })
  .catch(function () {
    console.log("Have ERROR here!!")
  })


let thirdStr = sendString('!!!!');
thirdStr
  .then(function (promiseString) {
    console.log(promiseString)
  })
  .catch(function () {
    console.log("Have ERROR here!!")
  })

  //--------------------Promise with .then-----------------------------------
  
  let trueOrFalse = true;

  let trueFalse = new Promise((resolve, reject) => {
    if(trueOrFalse == false){
      resolve("Resolved !! This is true!!")
    }else{
      reject("Rejected !! This is false!!")
    }
  })

  trueFalse
  .then(function(resolve){
    console.log("1. " + resolve + " 2. Passed! This is then")
  })
  .catch(function(reject){
    console.log("1. " + reject + " 2. Error! This is catch")
  })

  //------------------------Fetch URL with .then-----------------------------

  // let f = fetch("https://jsonplaceholder.typicode.com/users")

  // f
  // .then(function(data){
  //  return data.json();
  // })
  // .then(function(jsonData){
  //   console.log(jsonData);
  // })
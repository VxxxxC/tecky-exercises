
function sendString(): Promise<string> {
  return new Promise((resolve, reject) => {

    setTimeout(() => {
      resolve('Hello World!')
    }, 1500)
  })
}

async function printString() {
  let getString = await sendString()
  console.log(getString);
}
printString();

///////////////////////////////////////////////////////////////

async function getUser() {
  try {
    let username = await fetch('https://jsonplaceholder.typicode.com/users')
    username = await username.json();
    console.log(username)
  } catch (error) {
    console.log(`this is an ERROR : ${error}`)
  }
}

//----------------------Promise----------------------------------


function callname(name: string): Promise<string> {
  return new Promise((resolve, reject) => {
    resolve(name)
  })
}

callname("Hello").then(function (ok) {
  console.log(ok)
})
  .catch(function (err) {
    console.log(`we have error ${err}`)
  })


//----------------------Async Await with setTimeout----------------------------------


async function callNewName() {
  const name1 = await new Promise(function (resolve, reject) {
    setTimeout(function () {
      resolve("I'm name1 !!")
    }, 1500);
  })

  const name2 = await new Promise(function (resolve, reject) {

    resolve("I'm name2 !!")

  })

  console.log(name1)
  console.log(name2)
}

callNewName()

//----------------------Async Await with .then----------------------------------

async function callNameA() {
  return await new Promise(function (resolve, reject) {
    resolve("I am NameA !!")
  })
}
async function callNameB() {
  return await new Promise(function (resolve, reject) {
    resolve("I am NameB !!")
  })
}
async function callNameC() {
  return await new Promise(function (resolve, reject) {
    resolve("I am NameC !!")
  })
}

callNameA()
  .then(function (call) {
    console.log(call);
    return callNameB();
  })
  .then(function (call) {
    console.log(call);
    return callNameC();
  })
  .then(function (call) {
    console.log(call);
  })
  .catch(function (fail) {
    console.log(`catch fail msg!! ${fail}`)
  })

//----------------------Normal Function----------------------------------

function NewName() {
  const name5 = "I'm name 5!!"

  const name6 = "I'm name 6!!"

  console.log(name5)
  console.log(name6)
}

NewName()
import path from "path";
import fs from "fs";
import util from "util";

/*
function listAllJS(dirPath: any) {
  let currentPath = dirPath;
  // console.log(currentPath)

  fs.readdir(dirPath, (err, dir) => {

    if (err) {
      console.log(`this is an error from fs.readdir : ` + err);
      return;
    }

    //First layer Directory

    let dirArr: any[] = []
    dir.forEach(dirs => {
      dirArr.push(currentPath + "/" + dirs)
    })

    //Second layer directory

    for (let newDir of dirArr) {

      fs.stat(newDir, (err, stats) => {
        if (err) {
          console.log("we catch an ERROR!!: ", err)
        }
        if (stats.isFile()) {
          if (path.extname(newDir) === ".js") {
            console.log(newDir);
          }
        }

        if (stats.isDirectory()) {
          return listAllJS(newDir)
        }
      })

    }
  });
}

listAllJS('/Users/veperho/Desktop/gitlab-repo/tecky-exercise/Self_exercise');
*/

async function listJSfilePromise(dirPath: any): Promise<any> {
  return await new Promise<any>((resolve, reject) => {
    let currentPath = dirPath;

    fs.readdir(dirPath, (err, dir) => {
      if (err) {
        reject(`we catch an ERROR! : ${err}`)
      }

      //First directory layer
      const dirArr: any[] = [];
      dir.forEach(eachDir => {
        dirArr.push(currentPath + "/" + eachDir)
      })

      //Second or after directory layer
      for (let newDir of dirArr) {

        fs.stat(newDir, (err, stats) => {
          if (err) {
            console.log(`we got an ERROR on fs.stat!! : `, err)
          }
          if (stats.isFile()) {
            if (path.extname(newDir) === ".js") {
              resolve(newDir);
            }
          }
          if (stats.isDirectory()) {
            return listJSfilePromise(newDir)
          }
        })
      }
    })
  })
}

listJSfilePromise('/Users/veperho/Desktop/gitlab-repo/tecky-exercise/Self_exercise')
  .then(function (read) {
    console.log(`OK to read ${read}`)
  })
  .catch(function (fail) {
    console.log(`catched ERROR!! ${fail}`)
  })

async function writeJSfilePromise(filePath: any, source: any): Promise<string> {
  return await new Promise<string>((resolve, reject) => {
    fs.writeFile(filePath, source, (err) => {
      if (err) {
        reject(`we have error here! ${err}`)
      } else {
        resolve(`OK to write in to JSfile.txt`)
      }
    })
  })
}

writeJSfilePromise("./storage/JsFile.txt", listJSfilePromise)
  .then(function (write) {
    console.log(`OK to writed in to JsFile.txt ${write}`)
  })
  .catch(function (fail) {
    console.log(`we have an ERROR of writing!! ${fail}`)
  })
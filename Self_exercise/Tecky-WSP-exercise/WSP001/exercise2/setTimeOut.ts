type timeOut = () => void

const timeoutHandler : timeOut = () =>{
  console.log("Timeout happens!");
}

const timeout:number = 1000;

setTimeout(timeoutHandler,timeout);
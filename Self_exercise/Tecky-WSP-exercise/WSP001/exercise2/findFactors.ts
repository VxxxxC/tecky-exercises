function findFactors(num:number):number{
  let factors:Array<number> = [];
  for(let factor = 2; factor <= num / 2 ; factor++){
      if(num % factor === 0){
         factors.push(factor);
      }
  }
  return factors;
}

// require("ts-node/register");
// require("./app");

function acc() {
  let sum = 0;

  return function (num) {
    sum = sum + num;
    return sum;
  };
}



export let addsum = acc();

/** After declared export the addsum closure, below closure addsum cannot be console log in JS anymore,
 * but will the addsum will still keep counting on and contiues at TS side */

console.log(addsum(1));
console.log(addsum(10));
console.log(addsum(100));

import axios from 'axios';
import fs from 'fs';
import {lib} from './lib';
import Cls from './Cls';
import {func} from './func';
import {addsum} from './index.js'

/* lib is a name exported object from the module lib.*/
console.log(lib.someObject); /* should print "Hello World"*/
console.log(lib.someFunction()); /* should print "Foobar"*/
console.log(Cls()); /* Cls is a function which is exported as the default export of the module Cls*/
console.log(func()); /* func is a function which is exported as the named export of the module func */

console.log('fs:', typeof fs)
console.log('axios:', typeof axios)



/** After import the addsum closure from JS side , console log can only showing at TS now
    but the addsum numbers will start counting from JS side first */

console.log(addsum(1));
console.log(addsum(10));
console.log(addsum(100));
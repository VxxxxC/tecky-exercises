import express from 'express';
import { Client } from 'pg';
import jsonfile from 'jsonfile';
import path from 'path';
import formidable from 'formidable'
import http from 'http'
import { Server as ServerIO } from 'socket.io'
import bcrypt from 'bcryptjs'

const app = express();
const expressPort = 8020;

//---------------------Socket.io--------------------------
const server = new http.Server(app);
const io = new ServerIO(server);

io.on('connection', (socketio) => {
  console.log(`someone socket ID : "${socketio.id}" connected through socket`);


  io.emit('text', 'hello world')
  io.emit('text', 'this is server to client message...')

  socketio.on('client', (msg) => {
    console.log(msg);
  })
})

io.on('connection', (broadcast) => {
  broadcast.on('broadcast', (msg) => {
    console.log(msg)
  })
  io.emit('toClient', 'backend is broadcasting to client!!!');
})

//-----------------------Bcrypt hashing-----------------------------



async function hashPassword(password: string) {

  const SALT_ROUNDS = 10;

  const hash = await bcrypt.hash(password, SALT_ROUNDS);

  console.log(`Password was hashed to : ${hash}`)
  return hash;
}

async function checkPassword(password: string, hash: string) {
  const match = await bcrypt.compare(password, hash);

  console.log(match)
  return match;
}
//----------------------------------------------------------


const client = new Client({
  user: 'veper',
  password: 'abc123',
  database: 'memowall',
  host: 'localhost',
  port: 5432,
})
client.connect(err => {
  if (err) console.log("catch ERROR when connect to database! ", err)
  else console.log("connect database OK!")
});


app.use(express.static('public')) //TODO: 若然express.static未被COMMENT，下面所有路徑都會被攔截
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

//---------------- 用query方式 然後得到GET response-------------------- (query只可以GET)
// app.get('/', (req, res) => {
//   const name = req.query.name;
//   const location = req.query.location;
//   console.log(`Name is ${name}, Location is ${location}`);
//   res.end(`Name is ${name}, Location is ${location}`);
// });

// //---------------- 用params方式url 然後得到response--------------------
// app.get('/name/:name/loc/:location', (req, res) => {
//   const name = req.params.name;
//   const location = req.params.location;

//   console.log(`Name is ${name}, Location is ${location}`);
//   res.end(`Name is ${name}, Location is ${location}`);
// });

// //---------------- 用body方式url 然後得到POST response-------------------- (body只可以POST, PUT, DELETE)
// app.post('/', (req, res) => {
//   const { name, location } = req.body;
//   console.log(`Name is ${name}, Location is ${location}`);
//   res.end(`Name is ${name}, Location is ${location}`);
// });



//************************************************************************************/

app.post('/', async (req, res) => {
  // res.send("Your UserID " + req.body.username + " was registered") // FIXME: how to not conflict with res.redirect and operate together
  const { username, password } = req.body;
  await client.query(`INSERT INTO users (username,password,created_at,updated_at) VALUES ($1,$2,NOW(),NOW())`, [username, password]);
  console.log(req.body)
  res.redirect('/submit');
})

async function getUser() {
  let user: any = await client.query('SELECT * FROM users')
  // console.log("This is from Database : ", user.rows);
}
getUser();

app.get('/submit', (req, res) => {
  console.log(req.body)
  res.end('we have received your form submit, thank you!')
})

// app.get('/submit', (req, res) => {
//   res.send('You are submitted, thank you!')
//   // res.sendFile(path.join(__dirname, 'response.txt'))
//   // res.json({ message: " we received your form!" });
// })


// app.listen(expressPort, () => {
//   console.log(`Express server started, listening on localhost port: ` + expressPort);
// });

server.listen(expressPort, () => {
  console.log(`socket.io started, listening on localhost port: ` + expressPort);
});
const socket = io.connect();

socket.on("text", (socket) => {
  console.log(socket);
});

socket.emit("client", "hello world");
socket.emit("client", "this is client to server message...");

socket.emit("broadcast", "client is broadcasting to backend!!");

socket.on("toClient", (msg) => {
  console.log(msg);
});

document.querySelector("#contact-form").addEventListener("submit", async () => {
  console.log("form submitted!");
  const res = await fetch("/");
  const submit = await res.json();
  console.log(submit);
});

// document.querySelector(".broadcast").addEventListener("click", async () => {
//   console.log("you're going to broadcast!");
//   const res = await fetch("/broadcast");
//   const submit = await res.json();
//   console.log(submit);
// });

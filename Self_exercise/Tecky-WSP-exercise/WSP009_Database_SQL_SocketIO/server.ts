import express from 'express';
import expressSession from 'express';
import { Request, Response } from 'express'
import { print } from 'listening-on'
import fs, { mkdir, writeFile } from 'fs';
import jsonfile from 'jsonfile';
import path from 'path';
import formidable from 'formidable';
import pg from 'pg';
import { Client } from 'pg';
import dotenv from 'dotenv';
import xlsx from 'xlsx';
import { log } from 'console';


const PORT = 8000;
const app = express()


app.use(express.static('public'))
// app.use(express.static('/upload'))

app.use(express.urlencoded({
  extended: true
})); // Request ==> body

app.use(express.json()); // Request ==> json
app.use(expressSession())


dotenv.config()
const result = dotenv.config();
if (result.error) {
  console.log(result.error)
} else {
  console.log("dotenv config :", result)
}
const client = new Client({
  database: "memowall",
  user: "veper",
  password: "abc123",
  port: 5432,
  host: 'localhost',
});

interface User {
  username: String,
  password: String
}

interface Memo {
  content: String;
  image?: String
}


async function main() {
  try {
    await client.connect();

    // const user = {
    //   username: userID,
    //   password: num,
    // }

    let worksheet = xlsx.readFile('WSP009-exercise.xlsx')
    // console.log(worksheet.Sheets)
    let user: User[] = xlsx.utils.sheet_to_json(worksheet.Sheets.user);
    let memo: Memo[] = xlsx.utils.sheet_to_json(worksheet.Sheets.memo);
    // console.log(user)
    // console.log(memo)


    await client.query("truncate table users RESTART IDENTITY;");
    for (let users of user) {
      await client.query(
        'INSERT INTO users (username, password) values ($1,$2, current_timestamp, current_timestamp)',
        [users.username, users.password]
      )
    }
    await client.query("truncate table memos RESTART IDENTITY;");
    for (let memos of memo) {
      await client.query(
        "INSERT INTO memos (content,image, create_at, update_at) values ($1,$2, current_timestamp, current_timestamp)",
        [memos.content, memos.image]
      );
    }
    // const result = await client.query(
    //   'SELECT * from users where username = $1',
    //   [user.username]
    // )
    // console.log(result.rows[0].username)

  }
  catch (err) {
    console.log(err)
  }
  finally {
    await client.end()
  }
}
main()


// for (let worksheetUserInfo of worksheetUser) {
//   let loginUser = worksheetUserInfo.username;
//   let loginPs = parseInt(worksheetUserInfo.password);
// }


// for (let worksheetMemoInfo of worksheetMemo) {
//   console.log(worksheetMemoInfo.content)
//   let memotext = worksheetMemoInfo.content
//   console.log(memotext)
// }



declare module 'express-session' {
  interface SessionData {
    name?: string;
  }
}


const uploadDir = 'upload';
fs.mkdirSync(uploadDir, { recursive: true })
const form = formidable({
  uploadDir,
  keepExtensions: true,
  allowEmptyFiles: false,
  maxFiles: 1,
  maxFileSize: 1024 * 1024 ** 2,
  filter: file => file.mimetype?.startsWith('image/') || false,
})

// app.post('/', (req, res) => {
//   form.parse(req, (err, fields, files) => {
//     console.log({ err, fields, files })
//   })
// })

app.post('/', (req, res) => {
  // console.log(req.body)
  res.json({ message: "we receive your login info" })
})


app.post('/second', (req, res,) => {
  console.log(req.body)
  res.send('Form Submitted!')
})



// app.post('/', async (req: express.Request, res: express.Response) => {
//   let memoinput: any = req.body.memoinput
//   memoinput = await jsonfile.writeFile('memo.json', memoinput, { flag: 'w' }, (err) => {
//     if (err) {
//       console.error(err);
//     }
//     else {
//       console.log(`writing in memo.json is oK!!!!`)
//     }
//   })
// })


// app.get('/', async (req, res) => {
//   const readJson = await jsonfile.readFile('memo.json')
//   res.json(readJson)
// })

// app.post('/', async (req, res) => {
//   let writememo: any = req.body.memobox1
//   writememo = await writeFile(writememo, 'memo.json', (err) => {
//     if (err) {
//       console.error(err);
//     }
//     else {
//       console.log("OK to wirte on memo wall !!!!")
//     }
//   })
// })

// app.post('/', async (req: Request, res: Response,) => {
//   res.end(`here is memo directory...`);
//   const memo = await jsonfile.readFile('memo.json')
//   res.json(memo)

//   // const newMemo = req.body;
//   // newMemo.content = newMemo.image;
//   const { text } = req.body;
//   memo.push({
//     text: text,
//   });
//   jsonfile.writeFile("memo.json", memo, err => {
//     if (err) {
//       console.log(err);
//       return;
//     } // document.querySelector('#memowall').submit()
//   })

// });


// app.get('/', (req, res,) => {
//   console.log("Memo Request :", {
//     method: req.method,
//     url: req.url,
//   })
// })

app.listen(PORT, () => {
  console.log("server started")
  print(PORT)
})
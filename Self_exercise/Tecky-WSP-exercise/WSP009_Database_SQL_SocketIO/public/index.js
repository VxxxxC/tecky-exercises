document
  .querySelector("#memowall")
  .addEventListener("submit", async function (event) {
    event.preventDefault();

    // Serialize the Form afterwards
    const form = event.target;
    const formData = new FormData();

    formData.append("memoinput", form.memoinput.value);
    formData.append("memofile", form.memofile.files[0]);

    const res = await fetch("/", {
      method: "POST",
      body: formData,
    })
      .then((response) => {
        return response;
      })
      .catch(function (error) {
        console.log(error.error());
      })
      .then(function (result) {
        console.log(result);
      });
    if (form.filetype === "file") {
      document.querySelector(".imagebox").innHTML = await res.json();
    } else {
      document.querySelector(".textbox").innHTML = await res.json();
    }
  });

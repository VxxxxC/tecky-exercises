import express from 'express';
import expressSession from 'express';
import { Request, Response } from 'express'
import { print } from 'listening-on'
import fs, { mkdir, writeFile } from 'fs';
import jsonfile from 'jsonfile';
import path from 'path';
import formidable from 'formidable';

const user = {
  "username": "veper",
  "password": "abcdefg"
}

const secret: string = ''

const PORT = 8080;
const app = express()


app.use(express.static('index'))
// app.use(express.static('/upload'))

app.use(express.urlencoded({
  extended: true
})); // Request ==> body

app.use(express.json()); // Request ==> json

app.use(expressSession())

declare module 'express-session' {
  interface SessionData {
    name?: string;
  }
}


const uploadDir = 'upload';
fs.mkdirSync(uploadDir, { recursive: true })
const form = formidable({
  uploadDir,
  keepExtensions: true,
  allowEmptyFiles: false,
  maxFiles: 1,
  maxFileSize: 1024 * 1024 ** 2,
  filter: file => file.mimetype?.startsWith('image/') || false,
})

app.post('/', (req, res) => {
  form.parse(req, (err, fields, files) => {
    console.log({ err, fields, files })
  })
})



app.post('/second', (req, res,) => {
  console.log(req.body)
  res.end('Form Submitted!')
})



// app.post('/', async (req: express.Request, res: express.Response) => {
//   let memoinput: any = req.body.memoinput
//   memoinput = await jsonfile.writeFile('memo.json', memoinput, { flag: 'w' }, (err) => {
//     if (err) {
//       console.error(err);
//     }
//     else {
//       console.log(`writing in memo.json is oK!!!!`)
//     }
//   })
// })


// app.get('/', async (req, res) => {
//   const readJson = await jsonfile.readFile('memo.json')
//   res.json(readJson)
// })

// app.post('/', async (req, res) => {
//   let writememo: any = req.body.memobox1
//   writememo = await writeFile(writememo, 'memo.json', (err) => {
//     if (err) {
//       console.error(err);
//     }
//     else {
//       console.log("OK to wirte on memo wall !!!!")
//     }
//   })
// })

// app.post('/', async (req: Request, res: Response,) => {
//   res.end(`here is memo directory...`);
//   const memo = await jsonfile.readFile('memo.json')
//   res.json(memo)

//   // const newMemo = req.body;
//   // newMemo.content = newMemo.image;
//   const { text } = req.body;
//   memo.push({
//     text: text,
//   });
//   jsonfile.writeFile("memo.json", memo, err => {
//     if (err) {
//       console.log(err);
//       return;
//     } // document.querySelector('#memowall').submit()
//   })

// });


// app.get('/', (req, res,) => {
//   console.log("Memo Request :", {
//     method: req.method,
//     url: req.url,
//   })
// })

app.post('/contact', (req, res) => {
  // Console log the request body to see what is inside!
  console.log(req.body)
})

app.listen(PORT, () => {
  print(PORT)
})

# ------------------ List(Array) & Dictionary(Object) comprehension-----------------------

peoples = [
  {"name": "Alex", "height": 179, "hobbies": ["Eating", "Sleeping"]},
  {"name": "Gordon", "height": 180, "hobbies": ["Movie"]},
  {"name": "Michael", "height": 185, "hobbies": ["Eating"]},
  {"name": "Jason", "height": 170, "hobbies": []},
]

for people in peoples:
  if people["height"] > 170 and "Eating" in people["hobbies"]:
    print(people['name'])

print(
  [people['name']for people in peoples if people['height'] > 170 and 'Eating' in people['hobbies']]
  )

#********************************************************
print(('-' * 10) + 'This is isolate line' + ('-' * 10))
#********************************************************

# ----------------------------- Define (function)---------------------

bonus_people = [
  {"name": "Alex", "Years of work": 6, "Performance": "Poor", "Monthly salary": 10000},
  {"name": "Gordon", "Years of work": 5.5, "Performance": "Good", "Monthly salary": 40000},
  {"name": "Michael", "Years of work": 3, "Performance": "Good", "Monthly salary": 80000},
  {"name": "Jason", "Years of work": 7, "Performance": "Good", "Monthly salary": 70000},
  {"name": "Brian", "Years of work": 0.5, "Performance": "Good", "Monthly salary": 20000}
]
def bonus(list):
  total_bonus = 0
  for people in list:
    if people["Years of work"] > 1:
      if people["Years of work"] > 5 and people["Performance"] is "Good":
        print(people["name"] + " " + str(people["Monthly salary"]*2))
        total_bonus += people["Monthly salary"]*2
      else:
        print(people["name"] + " " + str(people["Monthly salary"]*1))
        total_bonus += people["Monthly salary"]*1

  print "total bonus of year :", total_bonus

    
    # print(people)
    # if people['Years of work'] > 1:
    #  print(str(people['name']) + ' got ' + str(people['Monthly salary'] * 1) + ' of 1x monthly salary bonus')     
    # if people['Years of work'] > 5 and people["Performance"] is "Good":
    #  print(str(people['name']) + ' got ' + str(people['Monthly salary'] * 2) + ' of 2x monthly salary bonus')

bonus(bonus_people)
a = 12
# Convert a into float
print(float(12))

pi = 3.141592654
# Convert pi into int
print(int(pi))

number_str= "6174.6174"
# Convert number_str into int
number_float = float(number_str)
number_int = int(number_float)
print(number_int)

true_str = "True"
# Convert true_str into boolean
true_str = True
print(true_str)


none = None
# Convert none into str
str(none)
print(none)

false = False
# Convert false into str
str(false)
print(false)

#********************************************************
print(('-' * 10) + 'This is isolate part 1 and part 2' + ('-' * 10))
#********************************************************

print(str("False"))
# boolean value of "False" string


print(10.0 / 3.0)
# result of 10 / 3

print(int("10") + int("3"))
# result of adding "10" + "3"

print(int("10") + 3)
# result of adding "10" + "3"

# print( True * None ) # This implementation is wrong
# result of any operating with None
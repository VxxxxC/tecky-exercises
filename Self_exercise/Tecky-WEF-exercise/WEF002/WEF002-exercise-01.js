const data = [
  {
    name: "Hong Kong",
    topLevelDomain: [".hk"],
    alpha2Code: "HK",
    alpha3Code: "HKG",
    callingCodes: ["852"],
    capital: "City of Victoria",
    altSpellings: ["HK", "香港"],
    region: "Asia",
    subregion: "Eastern Asia",
    population: 7324300,
    latlng: [22.25, 114.16666666],
    demonym: "Chinese",
    area: 1104.0,
    gini: 53.3,
    timezones: ["UTC+08:00"],
    borders: ["CHN"],
    nativeName: "香港",
    numericCode: "344",
    currencies: [
      {
        code: "HKD",
        name: "Hong Kong dollar",
        symbol: "$",
      },
    ],
    languages: [
      {
        iso639_1: "en",
        iso639_2: "eng",
        name: "English",
        nativeName: "English",
      },
      {
        iso639_1: "zh",
        iso639_2: "zho",
        name: "Chinese",
        nativeName: "中文 (Zhōngwén)",
      },
    ],
    translations: {
      de: "Hong Kong",
      es: "Hong Kong",
      fr: "Hong Kong",
      ja: "香港",
      it: "Hong Kong",
      br: "Hong Kong",
      pt: "Hong Kong",
      nl: "Hongkong",
      hr: "Hong Kong",
      fa: "هنگ‌کنگ",
    },
    flag: "https://restcountries.eu/data/hkg.svg",
    regionalBlocs: [],
    cioc: "HKG",
  },
];

const hk = data[0];

// console.log(Array.isArray(hk))
// console.log(hk instanceof Object)
// console.log(hk)

for (let key in hk) {

    if (key === 'currencies') {
        if(Array.isArray(hk.currencies) && (hk.currencies instanceof Object)) {
            for(let curr of hk.currencies) {
                for(let currKey in curr){
                    console.log(`currencies: ${currKey}: ${curr[currKey]}`)
                }             
            }
        }

    if (key == 'languages') {
        if(Array.isArray(hk.languages) && (hk.languages instanceof Object)) {
            for(let lang of hk.languages) {
                for(let langKey in lang){
                    console.log(`languages: ${langKey}: ${lang[langKey]}`)
                }             
            }
        }
        
    } else {
        console.log(key + ": " + hk[key]);
    }
    }
}

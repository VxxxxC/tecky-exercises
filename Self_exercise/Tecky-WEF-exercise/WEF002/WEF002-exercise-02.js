const cards = [
  ["Spade", "A"],
  ["Diamond", "J"],
  ["Club", "3"],
  ["Heart", "6"],
  ["Spade", "K"],
  ["Club", "2"],
  ["Heart", "Q"],
  ["Spade", "6"],
  ["Heart", "J"],
  ["Spade", "10"],
  ["Club", "4"],
  ["Diamond", "Q"],
  ["Diamond", "3"],
  ["Heart", "4"],
  ["Club", "7"],
];

console.log(`original list: `, cards);

console.log(
  `filter only "rank 10" cards: `,
  cards.filter((card) => card.includes("10"))
);

console.log(
  `Swap Diamond and Heart card: `,
  cards.map((str) => str.replace("Diamond", ""))
);


// let checkSpade = cards.reduce(function (acc, current) {
//   if (current[0] == "Spade") {
//     return acc + 1;
//   } else {
//     return acc;
//   }
// }, 0);

// console.log(`Spade have ${checkSpade} cards`);

// let club3Card = cards.filter(function (card) {
//   return card[0] !== "Club" && card[1] > 3 && card[1] == "J", "Q", "K", "A";
// });

// console.log(club3Card);

// let checkLarge = cards.reduce(function (acc, current) {
//   if (current[0] === "Diamond" && "Heart") {
//     if ((current[1] === "Q", "K", "A")) {
//       return acc + 1;
//     }
//   }
//   return acc;
// }, 0);

// console.log(`Diamond and Heart had ${checkLarge} cards large than J`);

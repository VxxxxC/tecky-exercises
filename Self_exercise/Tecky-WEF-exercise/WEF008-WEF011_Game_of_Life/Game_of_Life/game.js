let countNumber = document.querySelector(".countnumber");
let population = document.querySelector(".popname");
let cellColorText = document.querySelector(".colorname");
let cellColor = "rgb(0,255,0)";
let fpsNum = document.querySelector(".fpsNum");
let speed = document.querySelector(".speed");

const unitLength = 15;
const boxColor = 10;
const strokeColor = 10;
let columns; /* To be determined by window width */
let rows; /* To be determined by window height */
let currentBoard;
let nextBoard;

function setup() {
  /* set initial frame rate */
  frameRate(speed.valueAsNumber);

  /* set frameRate with DOM slider */
  speed.addEventListener("input", function () {
    frameRate(speed.valueAsNumber);
  });

  /* Set the canvas to be under the element #canvas*/
  const canvas = createCanvas(windowWidth, windowHeight - 150);
  canvas.parent(document.querySelector("#canvas"));

  /*Calculate the number of columns and rows */
  columns = floor(width / unitLength);
  rows = floor(height / unitLength);
  console.log(
    `have ${columns} columns and ${rows} rows = ${columns * rows} blocks`
  );

  /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
  currentBoard = [];
  nextBoard = [];
  for (let i = 0; i < columns; i++) {
    currentBoard[i] = [];
    nextBoard[i] = [];
  }
  // Now both currentBoard and nextBoard are array of array of undefined values.
  init(); // Set the initial values of the currentBoard and nextBoard
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight - 100);
}

function init() {
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      currentBoard[i][j] = {
        alive: false,
        age: "0",
      };
      nextBoard[i][j] = {
        alive: false,
        age: "0",
      };
    }
  }
}

// function start() {
//   for (let i = 0; i < columns; i++) {
//     for (let j = 0; j < rows; j++) {
//       currentBoard[i][j] = random() > 0.85 ? 1 : 0;
//     }
//   }
// }

function draw() {
  background(10);
  generate();
  let life = 0;
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      if (currentBoard[i][j].alive == true) {
        fill(cellColor);
        life++;
      } else {
        fill(10);
      }
      stroke(strokeColor);
      rect(i * unitLength, j * unitLength, unitLength, unitLength);
    }
  }
  countNumber.textContent = life;
}

function generate() {
  //Loop over every single box on the board
  for (let x = 0; x < columns; x++) {
    for (let y = 0; y < rows; y++) {
      // Count all living members in the Moore neighborhood(8 boxes surrounding)
      let neighbors = 0;
      for (let i of [-1, 0, 1]) {
        for (let j of [-1, 0, 1]) {
          if (i == 0 && j == 0) {
            // the cell itself is not its own neighbor
            continue;
          }
          // The modulo operator is crucial for wrapping on the edge
          // currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
          let nx = (x + i + columns) % columns;
          let ny = (y + j + rows) % rows;
          let nCell = currentBoard[nx][ny];
          if (nCell.alive) {
            neighbors += 1;
          }
        }
      }

      // Rules of Life
      if (currentBoard[x][y].alive == true && neighbors < 2) {
        nextBoard[x][y].alive = false;
        countNumber.textContent--;
      } else if (
        currentBoard[x][y].alive == true &&
        (neighbors == 2 || neighbors == 3)
      ) {
        nextBoard[x][y].alive = true;
        countNumber.textContent++;
      } else if (currentBoard[x][y].alive == true && neighbors > 3) {
        nextBoard[x][y].alive = false;
        countNumber.textContent--;
      } else if (currentBoard[x][y].alive == false && neighbors == 3) {
        nextBoard[x][y].alive = true;
        countNumber.textContent++;
      } else {
        nextBoard[x][y].alive = currentBoard[x][y].alive;
        currentBoard[x][y].age++;
      }
    }
  }

  // Swap the nextBoard to be the current Board
  [currentBoard, nextBoard] = [nextBoard, currentBoard];
  fpsNum.innerHTML = Math.ceil(frameRate());
}

/**
 * When mouse is dragged
 */
function mouseDragged() {
  /**
   * If the mouse coordinate is outside the board
   */
  if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
    return;
  }
  const x = Math.floor(mouseX / unitLength);
  const y = Math.floor(mouseY / unitLength);

  if (currentBoard[x][y].alive == true) {
    return;
  }
  currentBoard[x][y].alive = true;
  countNumber.textContent++;

  fill(cellColor);
  stroke(strokeColor);
  rect(x * unitLength, y * unitLength, unitLength, unitLength);
  console.log(mouseX, mouseY);
}

/**
 * When mouse is pressed
 */
function mousePressed() {
  noLoop();
  mouseDragged();
}

/**
 * When mouse is released
 */
function mouseReleased() {
  loop();
  mouseDragged();
}

// /** Below is for start button */
// document.querySelector("#start").addEventListener("click", function () {
//   start();
// });

/** Below is for reset button */
document.querySelector("#reset").addEventListener("click", function () {
  init();
  countNumber.textContent = 0;
  population.innerHTML = "";
});

////////////////////////////////////////////////////////////////////////

/**
 * Making initial population selector of group button
 */

const buttons = document.querySelectorAll(".btn-secondary");
buttons.forEach(function (currentBtn) {
  currentBtn.addEventListener("click", function () {
    if (currentBtn.value == "Rare") {
      population.innerHTML = "Rare";
      for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
          currentBoard[i][j].alive = random() > 0.9 ? true : false;
        }
      }
    } else if (currentBtn.value == "Normal") {
      population.innerHTML = "Normal";
      for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
          currentBoard[i][j].alive = random() > 0.85 ? true : false;
        }
      }
    } else if (currentBtn.value == "Intensive") {
      let yes = confirm("小心當機！");

      if (yes) {
        population.innerHTML = "Intensive";
        for (let i = 0; i < columns; i++) {
          for (let j = 0; j < rows; j++) {
            currentBoard[i][j].alive = random() > 0.55 ? true : false;
          }
        }
      } else {
        return;
      }
    }
  });
});

const colorButtons = document.querySelectorAll(".colorgp");
colorButtons.forEach(function (currentBtn) {
  currentBtn.addEventListener("click", function () {
    if (currentBtn.value == "Red") {
      cellColor = "rgb(255,0,0)";
      cellColorText.innerHTML = "Red";
      cellColorText.style.color = "red";
    } else if (currentBtn.value == "Yellow") {
      cellColor = "rgb(255,255,0)";
      cellColorText.innerHTML = "Yellow";
      cellColorText.style.color = "yellow";
    } else if (currentBtn.value == "Green") {
      cellColor = "rgb(0,255,0)";
      cellColorText.innerHTML = "Green";
      cellColorText.style.color = "green";
    } else if (currentBtn.value == "Blue") {
      cellColor = "rgb(0,0,255)";
      cellColorText.innerHTML = "Blue";
      cellColorText.style.color = "blue";
    } else if (currentBtn.value == "Purple") {
      cellColor = "rgb(255,0,255)";
      cellColorText.innerHTML = "Purple";
      cellColorText.style.color = "purple";
    }
  });
});

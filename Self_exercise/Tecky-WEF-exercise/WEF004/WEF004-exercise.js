let boxes = document.querySelectorAll(".box");
let restart = document.querySelector(".restart");
let apoint = document.querySelector(".apoint");
let bpoint = document.querySelector(".bpoint");

let clickTime = 0;
let yourTurn = 0;

for (let box of boxes) {
  box.addEventListener("click", function (event) {
    let selfbox = event.currentTarget;

    if (!selfbox.innerHTML) {
      if (yourTurn == 0) {
        box.innerHTML = '<i class="bi bi-x" style="font-size: 8rem"></i>';
        yourTurn += 1;
        apoint.textContent = +apoint.textContent + 1;
      } else {
        box.innerHTML = '<i class="bi bi-circle" style="font-size: 5rem"></i>';
        yourTurn -= 1;
        bpoint.textContent = +bpoint.textContent + 1;
      }

      clickTime++;
    }

    console.log(`this is yourTurn:${yourTurn}`);
    console.log(`this is clicktime: ${clickTime}`);
  });
}

for (let boxArr = 0; boxArr < boxes.length; boxArr++) {
  console.log(boxArr);
}

import React from "react";
import style from "./navbar.module.css";
import { Link } from "react-router-dom";
import TodoList from "./todoList";
import Login from "./login";
import Reminder from "./reminder";

function Navbar() {
  return (
    <div className={style.navBar}>
      <div className={style.navBarLeft}>
        <Link to="/" className={style.leftBtn}>
          Todo List
        </Link>
        <Link to="/reminder" className={style.leftBtn}>
          Reminder
        </Link>
      </div>

      <div className={style.navBarRight}>
        <Link to="/login" className={style.rightBtn}>
          Login
        </Link>
      </div>
    </div>
  );
}

export default Navbar;

import React from "react";
import style from "./reminder.module.css";

function Reminder() {
  return (
    <>
      <fieldset>
        <legend className={style.container}>
          <p>Reminder List</p>
        </legend>

        <div>
          <p></p>
        </div>

        <div className={style.listContent}>
          <ul>
            <li>Item 1</li>
            <li>Item 2</li>
            <li>Item 3</li>
          </ul>
        </div>
      </fieldset>
    </>
  );
}

export default Reminder;

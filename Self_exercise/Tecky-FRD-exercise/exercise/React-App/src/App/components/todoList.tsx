import React, { useState, useEffect } from "react";
import style from "./todoList.module.css";
import List from "./list";

export default function TodoList() {
  function Title(word: any) {
    return word;
  }

  const word = "TODO LIST";

  const [items, setItems] = useState<any[]>([]);

  // useEffect(() => {
  //   console.log(items);
  // }, []);
  // 空的dependency array，可以令useEffect只會在最初頭第一次render時觸發，往後render不會觸發useEffect

  const [input, setInput] = useState("");
  function inputChange(event: any) {
    setInput(event.target.value);
  }

  const [date, setDate] = useState<any[number]>();
  function dateChange(event: any) {
    setDate(event.target.value);
  }

  const [time, setTime] = useState<any[number]>();
  function timeChange(event: any) {
    setTime(event.target.value);
  }

  function add() {
    if (!input) {
      return;
    }
    setItems([...items, [input, date, time]]);
    // setInput([...input,setInput])
    // setDate([...date,setDate])
    // setTime([...time,setTime])

    setInput("");
  }

  function del() {
    items.pop();
    setItems([...items]);
  }

  // --------------CSS Style , 用引號變成全部String type--------------
  // let addBtnSty = {
  //   margin: "20px",
  //   color: "white",
  //   border: "1px solid white",
  //   "border-radius": "50px",
  //   "font-size": "18px",
  //   height: "36px",
  //   width: "40vw",
  // };

  // --------------CSS Style , 去除「-」號，用大楷寫法-------------------
  // let delBtnSty = {
  //   margin: "20px",
  //   border: "1px solid white",
  //   borderRadius: "50px",
  //   fontSize: "18px",
  //   color: "white",
  //   height: "30px",
  //   width: "40vw",
  // };

  return (
    <div className={style.container}>
      <div className={style.todoList}>
        <fieldset>
          <legend className={style.header}>{Title(word)}</legend>

          <div className={style.listItem}>
            <p>item : </p>
            <input
              className={style.inputText}
              type="text"
              value={input}
              onChange={inputChange}
            />
            <p>date : </p>
            <input
              className={style.inputText}
              type="date"
              value={date}
              onChange={dateChange}
            />
            <p>time : </p>
            <input
              className={style.inputText}
              type="time"
              value={time}
              onChange={timeChange}
            />
          </div>

          <div className="content">
            <button
              className={`btn btn-primary opacity-50 hover:opacity-80 ${style.addBtn}`}
              onClick={add}
            >
              Add
            </button>

            <ul>
              <List items={items} />
            </ul>

            <button
              className={`btn btn-error opacity-50 hover:opacity-80 ${style.delBtn}`}
              onClick={del}
            >
              Delete
            </button>
          </div>
        </fieldset>
      </div>
    </div>
  );
}

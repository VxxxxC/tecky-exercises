import React, { useState, useEffect, useRef } from "react";
import style from "./login.module.css";
import bcrypt from "bcryptjs";

async function hash(password: any) {
  const salt: any = bcrypt.genSaltSync(10);

  const hash: any = bcrypt.hashSync(password, salt);

  return hash;
}

// FIXME: still have bug, hash process got error in the middle
// hash("123456").then((hash: any) => {
//   console.log(hash);
// });
// console.log(hash("123456"));

function Login() {
  // const userRef = useRef();
  // const errRef = useRef();

  const [username, setUsername] = useState("");
  function usernameInput(event: any) {
    setUsername(event.target.value);
  }

  const [password, setPassword] = useState("");
  function passwordInput(event: any) {
    setPassword(event.target.value);
  }

  // useEffect(() => {
  //   console.log(userRef);
  //   console.log(errRef);
  // }, []);

  async function handleSubmit(event: any) {
    event.preventDefault();

    let User = { username, password };

    console.log(User);

    setUsername("");
    setPassword("");
  }

  return (
    <>
      <div className={style.container}>
        <h2 className={style.h2}>Login Page</h2>
        <form name="login">
          <div>
            <label>Username</label>
            <input
              type="text"
              name="username"
              placeholder="Enter your username"
              value={username}
              onChange={usernameInput}
            />
          </div>
          <div>
            <label>Password</label>
            <input
              type="password"
              name="password"
              placeholder="Enter your password"
              value={password}
              onChange={passwordInput}
            />
          </div>
          <button
            onClick={handleSubmit}
            className={style.submitBtn}
            type="submit"
            name="login"
          >
            Submit
          </button>
        </form>
      </div>
    </>
  );
}

export default Login;

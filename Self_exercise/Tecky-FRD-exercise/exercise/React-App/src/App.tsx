import React from "react";
// import Center from "./App/center";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import Login from "./App/components/login";
import Navbar from "./App/components/navbar";
import Reminder from "./App/components/reminder";
import TodoList from "./App/components/todoList";

function App() {
  return (
    <>
      <BrowserRouter>
        <Navbar />

        <Routes>
          <Route path="/" element={<TodoList />} />
          <Route path="/reminder" element={<Reminder />} />
          <Route path="/login" element={<Login />} />
        </Routes>
        
      </BrowserRouter>
    </>
  );
}

export default App;

import dotenv from 'dotenv';

dotenv.config()
const Config = dotenv.config({});
if (Config.error) {
   console.error("connect to DB have an error : ", Config.error)
} else {
   console.log("DB connected with config : ", Config);
}

export const env: any = {
   DB_NAME: process.env.DB_NAME,
   DB_USER: process.env.DB_USER,
   DB_PASSWORD: process.env.DB_PASSWORD,
   DB_HOST: process.env.DB_HOST,
   PORT: process.env.PORT,
}
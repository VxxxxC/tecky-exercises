import express from 'express';
import { client } from './db';

const app = express();

const PORT = 8080;

app.listen(PORT, () => {
   console.log('server connected to PORT : ', PORT)
});

client.connect(err => {
   if(err){
      console.error("DB connect with error : ", err);
   } else {
      console.log(`Database => ${client.database}, Port => ${client.port}, connected by => ${client.user}`)
   }
})
import express from 'express';
import permit, { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import { env } from './env';

export type JWTPayload = {
   id: number // user id
}

// declare module 'express' {
//    interface Request {
//       jstPayload: JWTPayload
//    }
// }

export function getJWTPayload(
   req: express.Request, res: express.Response, next: (payload: JWTPayload) => void
) {
   let token: string;

   try {
      token = permit.check(req)
   }
   catch (error) {
      res.status(401).json({ error: "missing bearer token" });
      return;
   }

   let payload: JWTload;
   try {
      payload = jwtSimple.decode(token, env.JWT_SECRET)
   } catch (error) {
      res.status(400).json({ error: "invalid JWT in bearer" });
   }

   next(payload);
}
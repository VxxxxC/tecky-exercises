import { env } from './env'
import { Client } from "pg";

export const client = new Client({
   database: env.DB_NAME,
   user: env.DB_USER,
   password: env.DB_PASSWORD,
})
